<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="mediatek2020.items.Utilisateur" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/body.css">
<script src="js/services.js"></script>
<title>Accueil</title>
</head>
<body>
<div id="menu">
	<%if(request.getRemoteAddr().equals("0:0:0:0:0:0:0:1")) {%>
	<button id="bouton__emprunter">Emprunter</button>
	<%}
	if(request.getRemoteAddr().equals("0:0:0:0:0:0:0:1")){ %>
	<button id="bouton__retourner">Retourner</button>
	<%} %>
	<%if(request.getRemoteAddr().equals("0:0:0:0:0:0:0:1")){ %>
	<button id="bouton__reserver">Reserver</button>
	<%} %>
	<%Utilisateur u = (Utilisateur)session.getAttribute("utilisateur");
		if((boolean)u.data()[3]){%>
		<button id="bouton__ajouter">Ajouter Document</button>
	<%} %>
	<button id="bouton__deconnexion">Déconnexion</button>
	</div>
	<%if(request.getAttribute("error")!=null) { %>
	<div><%=request.getAttribute("error") %></div>
	<%} %>
</body>
</html>