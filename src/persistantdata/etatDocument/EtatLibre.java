package persistantdata.etatDocument;

import mediatek2020.items.EmpruntException;
import mediatek2020.items.ReservationException;
import mediatek2020.items.RetourException;
import mediatek2020.items.Utilisateur;

public class EtatLibre extends EtatDocument {
	private int idDoc;

	public EtatLibre(int idDoc) {
		this.idDoc = idDoc;
	}

	@Override
	public EtatDocument emprunter(Utilisateur u) throws EmpruntException {
		return new EtatEmprunter(u, idDoc);
	}

	@Override
	public EtatDocument reserver(Utilisateur u) throws ReservationException {
		return new EtatReserver(u, idDoc);
	}

	@Override
	public EtatDocument retour(Utilisateur u) throws RetourException {
		throw new RetourException();
	}

	@Override
	public String getEtat() {
		return "Libre";
	}
	
	@Override
	public Utilisateur getProprio() {
		return null;
	}

}
