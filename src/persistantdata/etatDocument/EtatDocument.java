package persistantdata.etatDocument;

import mediatek2020.items.EmpruntException;
import mediatek2020.items.ReservationException;
import mediatek2020.items.RetourException;
import mediatek2020.items.Utilisateur;

public abstract class EtatDocument {
	public abstract EtatDocument emprunter(Utilisateur u) throws EmpruntException ;
	public abstract EtatDocument reserver(Utilisateur u) throws ReservationException ;
	public abstract EtatDocument retour(Utilisateur u) throws RetourException ;
	public abstract String getEtat();
	public abstract Utilisateur getProprio();
}
