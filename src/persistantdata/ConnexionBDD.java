package persistantdata;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;

public class ConnexionBDD {
	
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/projetappserv", "ProjetAppServ",
					"dLsChcT3PZopRyf2");
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Probl�me de connexion au serveur SQL");
			System.out.println(e.toString());
		}
		return null;
	}
}
