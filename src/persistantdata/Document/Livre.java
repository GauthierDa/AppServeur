package persistantdata.Document;

import persistantdata.etatDocument.EtatDocument;

public class Livre extends ADocument {

	public Livre(int idDoc, String nomAuteur, String titre, EtatDocument etatDocument) {
		super(idDoc, nomAuteur, titre, etatDocument);
	}
}
