package persistantdata.Utilisateur;

import mediatek2020.items.Utilisateur;

public class User implements Utilisateur {
	private int idUser;
	private String name;
	private String login;
	private boolean bibliothecaire;
	
	public User(int idUser, String name, String login, boolean bibliothecaire) {
		this.idUser = idUser;
		this.name = name;
		this.login = login;
		this.bibliothecaire = bibliothecaire;
	}
	@Override
	public Object[] data() {
		Object[] user = new Object[4];
		user[0] = idUser;
		user[1] = name;
		user[2] = login;
		user[3] = bibliothecaire;
		return user;
	}

	/**
	 * renvoie un true si bibliothecaire, sinon false
	 */
	@Override
	public boolean isBibliothecaire() {
		return this.bibliothecaire;
	}

	/**
	 * renvoie l'ID du bibliothecaire
	 */
	@Override
	public String name() {
		return name;
	}
}
