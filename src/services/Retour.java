package services;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mediatek2020.Mediatheque;
import mediatek2020.items.Document;
import mediatek2020.items.Utilisateur;

public class Retour extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Accueil.estConnecte(request, response);
		HttpSession session	= request.getSession(true);
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		List<Document> listDocuments = Mediatheque.getInstance().tousLesDocuments();
		List<Document> documentsaRendre = new LinkedList<Document>();
		for(Document d : listDocuments) {
			Object[] data = d.data();
			String etat = (String) data[3];
			if(etat.equals("Reserve") || etat.equals("Emprunte")){
				if(((Utilisateur) data[4]).data()[0] == utilisateur.data()[0]) {
					documentsaRendre.add(d);
				}
			}
		}
		request.setAttribute("listeDocument",documentsaRendre);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Retour.jsp").forward(request, response);
	}
}
