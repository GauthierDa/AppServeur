package services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mediatek2020.Mediatheque;

public class AjouterDocument extends HttpServlet {
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Accueil.estConnecte(request, response);
		String titre = request.getParameter("Titre");
		String auteur = request.getParameter("Auteur");
		int type = Integer.parseInt(request.getParameter("Type"));
		Mediatheque.getInstance().nouveauDocument(type, titre, auteur);
		response.sendRedirect("accueil");
	}
}
